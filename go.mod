module goservice

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.4.1
	github.com/joho/godotenv v1.4.0
)
