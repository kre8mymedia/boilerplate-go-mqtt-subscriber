package main

import (
    "fmt"
    "os"
    "os/signal"
    "syscall"
    services "goservice/services"
)

func main() {
    c := make(chan os.Signal, 1)
    signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	
    client := services.Subscription("read/cpu_temp/#")
    if token := client.Connect(); token.Wait() && token.Error() != nil {
            panic(token.Error())
    } else {
            fmt.Printf("Connected to server\n")
    }
    <-c
}