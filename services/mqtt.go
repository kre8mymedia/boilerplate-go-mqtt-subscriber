package services

import (
    "fmt"
    MQTT "github.com/eclipse/paho.mqtt.golang"
    "os"
    "log"
	"math/rand"
    "github.com/joho/godotenv"
)

var knt int
var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
    fmt.Printf("MSG: %s\n", msg.Payload())
    text := fmt.Sprintf("this is result msg #%d!", knt)
    knt++
    token := client.Publish("nn/result", 0, false, text)
    token.Wait()
}

func Subscription(topic string) MQTT.Client {
    err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Some error occured. Err: %s", err)
	}

    MQTT_HOST := os.Getenv("MQTT_HOST")
    MQTT_PORT := os.Getenv("MQTT_PORT")
    MQTT_USERNAME := os.Getenv("MQTT_USERNAME")
    MQTT_PASSWORD := os.Getenv("MQTT_PASSWORD")

    s := fmt.Sprintf("%v", rand.Float64())
    opts := MQTT.NewClientOptions()
    opts.AddBroker("tcp://" + MQTT_HOST + ":" + MQTT_PORT)
	opts.SetClientID(s)
    opts.SetUsername(MQTT_USERNAME)
    opts.SetPassword(MQTT_PASSWORD)
    opts.SetDefaultPublishHandler(f)
    // topic := "read/cpu_temp/#"

    opts.OnConnect = func(c MQTT.Client) {
            if token := c.Subscribe(topic, 0, f); token.Wait() && token.Error() != nil {
                    panic(token.Error())
            }
    }
    client := MQTT.NewClient(opts)
    return client
}